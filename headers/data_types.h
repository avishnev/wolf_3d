//
// Created by Oleksii on 21.02.2020.
//

#ifndef WOLF_3D_DATA_TYPES_H
#define WOLF_3D_DATA_TYPES_H

#include "SDL2/SDL.h"

typedef struct  s_vec2
{
	int x;
	int y;
}               t_vec2;

typedef struct  s_vec2f
{
	double  x;
	double  y;
}               t_vec2f;


typedef struct s_sprite
{
	t_vec2f     pos;
	int         health;
	int         type;
	int         isDead;
	int         spriteSize;
	int         animLength;
	double      dist;
	void        *pSpr;
	int         vDiv;
	int         uDiv;
	int         vMove;
	SDL_Surface **texture;
}               t_sprite;


#endif //WOLF_3D_DATA_TYPES_H
