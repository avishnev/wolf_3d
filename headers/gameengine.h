//
// Created by Oleksii on 16.01.2020.
//

#ifndef WOLF_3D_GAMEENGINE_H
# define WOLF_3D_GAMEENGINE_H

#include <SDL_surface.h>
#include <SDL_video.h>
#include <SDL2_ttf/SDL_ttf.h>
#include "defines.h"
#include "../sources/engine/sprites.h"
#include "../headers/data_types.h"

typedef struct s_raycast t_raycast;

typedef struct  s_engine
{
	t_list      *enemies;
	t_vec2      weaponPos;
	t_raycast   *rc;
	t_sprites   *res;
	SDL_Surface *surface;
	SDL_Surface *s_freew;

	SDL_Window  *window;
	TTF_Font    *font;
	int32_t     **mapType;
	int32_t     *pxls[10];

	uint32_t    frameBuffer[WIN_HG][WIN_WD];
	int         loaded;
	int         shootAnim;
}               t_engine;

#endif
