//
// Created by Oleksii on 16.01.2020.
//

#ifndef WOLF_3D_GAME_H
#define WOLF_3D_GAME_H

#include "gameengine.h"

typedef struct s_player
{
	t_vec2f position;
	t_vec2f direction;
	int     hp;
	int     ammo;
	int     weaponType;
	int     score;
}              t_player;

typedef struct s_game
{
	int         isClosedMap;
	int         gameMode;

	int         inMainMenu;
	t_engine    *engine;
	t_player    *player;

	t_vec2f plane;
}               t_game;

#endif //WOLF_3D_GAME_H
