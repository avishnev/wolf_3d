//
// Created by Oleksii on 16.01.2020.
//

#ifndef WOLF_3D_DEFINES_H
#define WOLF_3D_DEFINES_H

#define TEXTURERESOLUTION 64
#define NTEXTURERESOLUTION 64


#define WOLF_KEY e.key.keysym.scancode
#define PL_DIR_X pGame->player->direction.x
#define PL_DIR_Y pGame->player->direction.y
#define PL_POS_X pGame->player->position.x
#define PL_POS_Y pGame->player->position.y

#define FALSE 0
#define TRUE 1

#define WALL_FAM 0xfa
#define BRICK_WALL_RED 0x1f
#define GRAY_STONE 0x2f
#define BLUE_WALL 0x3f
#define HITLER 0x4f
#define ELEVATOR 0x5f
#define GRASS_DAY 0x6f
#define NIGHT_SHIT 0x7f
#define COMBINE_TEXTURE 0x8f
#define PENIS_HITLER 0x9f

#define STEEL_DOOR '-'
#define STEEL_DOOR_OPENED '/'

#define CAN_MOVE 0

#define WIN_WD 1024
#define WIN_HG 786

#define HALF_WD WIN_WD >> 1
#define HALF_HG WIN_HG >> 1


#define DOOR_F_N '1'
#define DOOR_F_S '2'
#define DOOR_F_W '3'
#define DOOR_F_E '4'



#define DOUBLEBARREL 0x1a
#define SHOTGUN 0x2a
#define CHAINSAW 0x3a

#define ENEMY_DEAMON 'e'
#define ENEMY_HITLER 'h'
#define WEAPON_C 'c'
#define WEAPON_S 's'
#define WEAPON_D 'd'



#define DR_SPRITE ((t_sprite *)it->content)
#define FRAME_B pGame->engine->frameBuffer
#define TICK (SDL_GetTicks() / 150)

#define PLAYER 10
#define MAX_MAP_SIZE 100

#define GREEN_C 6, 63, 63
#define RED_C 255, 0 ,0

#define OBSERVED_DIST  6.0f


#define WOLF_FREE(x) {if (x) {free(x); x = NULL;}}

#endif //WOLF_3D_DEFINES_H
