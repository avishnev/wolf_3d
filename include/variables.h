#ifndef VARIABLES_H

# define VARIABLES_H
# define SCR_WIDTH 800
# define SCR_HEIGTH 600

typedef unsigned int t_uint;

typedef struct		s_control
{
	SDL_Event	event;


}			t_control;

typedef struct s_engine
{
	SDL_Window	*window;
	SDL_Surface	*surf;
	t_control	wait;

}		t_engine;


typedef struct	s_vec
{
	double		x;
	double		y;
}				t_vec;


typedef struct	s_color
{
	int		r;
	int		g;
	int		b;	
}				t_color;

typedef struct	s_var
{
	t_vec	*player_pos;
	t_vec	*player_dir;
	t_vec	*camera_plane;
}				t_var;



#endif