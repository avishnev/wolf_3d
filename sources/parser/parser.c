//
// Created by Oleksii on 16.01.2020.
//
#include "IParser.h"

int32_t  *readLine(char *line)
{
	int32_t *res;
	size_t  i;

	res = (int32_t *)ft_memalloc(MAX_MAP_SIZE);
	i = -1;
	while (++i < ft_strlen(line))
	{
		if (!ft_iswhitesp(line[i]))
		{
			if (line[i] == '1')
				res[i] = BRICK_WALL_RED;
			else if (line[i] == '2')
				res[i] = GRAY_STONE;
			else if (line[i] == '3')
				res[i] = BLUE_WALL;
			else if (line[i] == '4')
				res[i] = HITLER;
			else if (line[i] == '5')
				res[i] = ELEVATOR;
			else if (line[i] == '6')
				res[i] = GRASS_DAY;
			else if (line[i] == '7')
				res[i] = NIGHT_SHIT;
			else if (line[i] == '8')
				res[i] = COMBINE_TEXTURE;
			else if (line[i] == '-')
				res[i] = STEEL_DOOR;
			else if (line[i] == 'x')
				res[i] = PLAYER;
			else if (line[i] == 'h')
				res[i] = ENEMY_HITLER;
			else if (line[i] == 'd')
				res[i] = WEAPON_D;
			else if (line[i] == 's')
				res[i] = WEAPON_S;
			else if (line[i] == 'c')
				res[i] = WEAPON_C;
			else if (line[i] == 'f')
				res[i] = WALL_FAM;
			else if (line[i] == 'e')
				res[i] = ENEMY_DEAMON;
			else
				res[i] = 0xff;
		}
		else
			res[i] = CAN_MOVE;
	}
	while (i++ < MAX_MAP_SIZE)
		res[i] = CAN_MOVE;
	return res;
}

int32_t     **createMapType(int fd)
{
	char    *line;
	int32_t **tab;
	int     i;

	i = 0;
	tab = (int32_t **)ft_memalloc(MAX_MAP_SIZE);

	while (ft_next_line(fd, &line) > 0)
	{
		if (i >= MAX_MAP_SIZE)
		{
			ft_putstr("Error: too big map");
			exit(EXIT_FAILURE);
		}
		tab[i++] = readLine(line);
	}
	while (i < MAX_MAP_SIZE)
		tab[i++] = ft_memalloc(MAX_MAP_SIZE);
	return tab;
}

void freeMapType(int32_t **pMapType)
{
	int i;

	i = -1;
	while (++i < MAX_MAP_SIZE)
		WOLF_FREE(pMapType[i]);
	WOLF_FREE(pMapType);
}
const char  *checkInput(int ac, const char **av)
{
	if (ac == 2)
		return (*(++av));
	else
	{
		ft_putstr("Error: wrong usage");
		exit(EXIT_FAILURE);
	}
}

int     open_file(const char *file)
{
	int fd;

	if (!file)
	{
		ft_putstr(strerror(errno));
		exit(EXIT_FAILURE);
	}
	if ((fd = open(file, O_RDONLY)) < 0)
	{
		ft_putstr(strerror(errno));
		exit(EXIT_FAILURE);
	}
	return (fd);
}
