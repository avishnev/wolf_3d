//
// Created by Oleksii on 16.01.2020.
//

#ifndef WOLF_3D_IPARSER_H
# define WOLF_3D_IPARSER_H

# include <errno.h>
# include "../../userlib/libft/sources/libft.h"
# include "../../headers/defines.h"

const char  *checkInput(int ac, const char **av);
int         open_file(const char *file);

int32_t     **createMapType(int fd);
int32_t     *readLine(char *line);
void        freeMapType(int32_t **pMapType);


#endif //WOLF_3D_IPARSER_H
