#include "../include/wolf_3d.h"
#include "../include/variables.h"
#include <math.h>
#include <stdio.h>

#define EXIT_GAME -1

#define TRUE 1
#define FALSE 0

#define mapWidth 24
#define mapHeight 24

int worldMap[mapWidth][mapHeight]=
{
  {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,2,2,2,2,2,0,0,0,0,3,0,3,0,3,0,0,0,1},
  {1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,3,0,0,0,3,0,0,0,1},
  {1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,2,2,0,2,2,0,0,0,0,3,0,3,0,3,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,4,0,4,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,4,0,0,0,0,5,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,4,0,4,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,4,0,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
  {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
};



int verLine(int x, int y1, int y2, t_color color, SDL_Surface *scr)
{
	if(y2 < y1)
	{
		y1 += y2;
		y2 = y1 - y2;
		y1 -= y2;
	} //swap y1 and y2
	if (y2 < 0 || y1 >= SCR_HEIGTH  || x < 0 || x >= SCR_WIDTH) 
		return 0; //no single point of tSCR_HEIGTHe line is on screen
	if(y1 < 0)
		y1 = 0; //clip
	if (y2 >= SCR_WIDTH)
		y2 = SCR_HEIGTH - 1; //clip

	Uint32 colorSDL = SDL_MapRGB(scr->format, color.r, color.g, color.b);
	Uint32* bufp;

	bufp = (Uint32*)scr->pixels + y1 * scr->pitch / 4 + x;
	for(int y = y1; y <= y2; y++)
	{
		*bufp = colorSDL;
		bufp += scr->pitch / 4;
	}
	return 1;
}

void	error_manadge(char *error_log, int flag, const char *add_info)
{
	if (error_log)
		printf("ERROR: %s\n", error_log);
	else if (add_info)
		printf("ERROR: %s\n",add_info);
	exit(flag);

}
 
int		init_data(t_var *data)
{
	if (!data)
		return (0);
	data->player_pos = (t_vec *)malloc(sizeof(t_vec));
	data->player_dir = (t_vec *)malloc(sizeof(t_vec));
	data->camera_plane = (t_vec *)malloc(sizeof(t_vec));

	if (!data->player_dir || !data->camera_plane || !data->player_pos)
		error_manadge("malloc error", -1, __func__);
	memset(data->player_pos, 0, sizeof(t_vec));
	memset(data->player_dir, 0, sizeof(t_vec));
	memset(data->camera_plane, 0, sizeof(t_vec));

	data->player_pos->x = 12;
	data->player_pos->y = 22;
	data->player_dir->x = -1;
	data->player_dir->y = 0;
	data->camera_plane->x = 0;
	data->camera_plane->y = 0.66;
	return (1);

}

void	init_engine(t_engine *engine, int fullscreen)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
		error_manadge((char *)SDL_GetError(), 0, NULL);
	if (!fullscreen)
	{
		engine->window = SDL_CreateWindow("WOLF_3D",
				SDL_WINDOWPOS_UNDEFINED,
				SDL_WINDOWPOS_UNDEFINED,
				SCR_WIDTH, SCR_HEIGTH,
				SDL_WINDOW_ALLOW_HIGHDPI);
	}
	else
	{
		engine->window = SDL_CreateWindow("WOLF_3D",
				SDL_WINDOWPOS_UNDEFINED,
				SDL_WINDOWPOS_UNDEFINED,
				SCR_WIDTH, SCR_HEIGTH,
				SDL_WINDOW_FULLSCREEN);
	}
	engine->surf = SDL_GetWindowSurface(engine->window);
}

int	event_check(t_engine *engine, t_var *var)
{
	(void)var;
	if (SDL_PollEvent(&engine->wait.event))
	{
		if (engine->wait.event.type == SDL_QUIT || 
		engine->wait.event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
			return (EXIT_GAME);
		else if (engine->wait.event.key.state == SDL_PRESSED && engine->wait.event.key.keysym.scancode == SDL_SCANCODE_UP )
		{
			printf("KEY == [%d] UP\n", engine->wait.event.key.keysym.scancode);
			return (0);
		}
		else if (engine->wait.event.key.state == SDL_PRESSED && engine->wait.event.key.keysym.scancode == SDL_SCANCODE_LEFT)
		{
			printf("KEY == [%d] LEFT\n", engine->wait.event.key.keysym.scancode);	
			return (0);
		}
		else if (engine->wait.event.key.state == SDL_PRESSED && engine->wait.event.key.keysym.scancode == SDL_SCANCODE_RIGHT)
		{
			printf("KEY == [%d] RIGHT\n", engine->wait.event.key.keysym.scancode);	
			return (0);
		}
		else if (engine->wait.event.key.state == SDL_PRESSED && engine->wait.event.key.keysym.scancode == SDL_SCANCODE_DOWN)
		{
			printf("KEY == [%d] DOWN\n", engine->wait.event.key.keysym.scancode);	
			return (0);
		}

	}
	return (0);
}

int main(int argc, char const *argv[])
{			

	t_engine	*engine = (t_engine *)malloc(sizeof(t_engine));
	t_var		*var = (t_var *)malloc(sizeof(t_var));
	init_engine(engine, FALSE);
	init_data(var);

	
	while (1)
	{
		if (event_check(engine, var) == EXIT_GAME)
			break ;
		SDL_UpdateWindowSurface(engine->window);

	}
	if (argc == 2)
		printf("{%s}\n", argv[1]);
	SDL_Quit();
	free(engine);
	return 0;
}
