//
// Created by Oleksii on 21.02.2020.
//

#include "IEvent_handler.h"
#include "utils.h"

void checkShotHitBox(t_game *pGame, int32_t **pMap)
{
	int front;
	int side;

	float i  = 0;
	front = pMap[(int)(PL_POS_X + PL_DIR_X * 3)][(int)PL_POS_Y];
	side = pMap[(int)PL_POS_X][(int)(PL_POS_Y + PL_DIR_Y * 3)];

	while (i < 6) {
		front = pMap[(int)(PL_POS_X + PL_DIR_X * i)][(int)PL_POS_Y];
		side = pMap[(int)PL_POS_X][(int)(PL_POS_Y + PL_DIR_Y * i)];
		if (side == ENEMY_HITLER || front == ENEMY_HITLER || side == ENEMY_DEAMON || front == ENEMY_DEAMON ) {
			fprintf(stderr, "HIIIIIIIT %d %d\n", side, front);
			break;
		}
		i += 0.5f;
	}

	fprintf(stderr, "plpos: %f:%f\n  f: pos %d:%d type %d,  s: pos %d:%d type %d\n",PL_POS_X, PL_POS_Y,
		(int)(PL_POS_X + PL_DIR_X * 6.0f), (int)PL_POS_Y, front, (int)PL_POS_X, (int)(PL_POS_Y + PL_DIR_Y * 6.0f), side);

//	if (front == ENEMY_HITLER)
//	{
//		pGame->player->score += 10;
//		pMap[(int)(PL_POS_X + PL_DIR_X * 1)][(int)PL_POS_Y] = PENIS_HITLER;
//	}
//	else if (side == HITLER)
//	{
//		pGame->player->score += 10;
//		pMap[(int)PL_POS_X][(int)(PL_POS_Y + PL_DIR_Y * 1)] = PENIS_HITLER;
//	}

}
void checkHit(t_game *pGame, int32_t **pMap)
{
	if (pMap[(int)(PL_POS_X + PL_DIR_X * 1)][(int)PL_POS_Y] == HITLER)
	{
		pGame->player->score += 10;
		pMap[(int)(PL_POS_X + PL_DIR_X * 1)][(int)PL_POS_Y] = PENIS_HITLER;
	}
	else if (pMap[(int)PL_POS_X][(int)(PL_POS_Y + PL_DIR_Y * 1)] == HITLER)
	{
		pGame->player->score += 10;
		pMap[(int)PL_POS_X][(int)(PL_POS_Y + PL_DIR_Y * 1)] = PENIS_HITLER;
	}
	if (pMap[(int)(PL_POS_X)][(int)(PL_POS_Y + PL_DIR_Y * 1)] == ELEVATOR)
	{
		////TODO: Download new Level
		fprintf(stderr, "\n\n////TODO: Download new Level\n\n");
	}
	else if (pMap[(int)(PL_POS_X + PL_DIR_X * 1)][(int)(PL_POS_Y)] == ELEVATOR)
	{
		fprintf(stderr, "\n\n////TODO: Download new Level\n\n");
	}

	if (pMap[(int)(PL_POS_X)][(int)(PL_POS_Y + PL_DIR_Y * 1)] == STEEL_DOOR)
	{
		pMap[(int)(PL_POS_X)][(int)(PL_POS_Y + PL_DIR_Y * 1)] = STEEL_DOOR_OPENED;
		return;
	}
	else if (pMap[(int)(PL_POS_X + PL_DIR_X * 1)][(int)(PL_POS_Y)] == STEEL_DOOR)
	{
		pMap[(int)(PL_POS_X + PL_DIR_X * 1)][(int)(PL_POS_Y)] = STEEL_DOOR_OPENED;
		return;
	}

	if (pMap[(int)(PL_POS_X)][(int)(PL_POS_Y + PL_DIR_Y * 1)] == STEEL_DOOR_OPENED)
	{
		pMap[(int)(PL_POS_X)][(int)(PL_POS_Y + PL_DIR_Y * 1)] = STEEL_DOOR;
	}
	else if (pMap[(int)(PL_POS_X + PL_DIR_X * 1)][(int)(PL_POS_Y)] == STEEL_DOOR_OPENED)
	{
		pMap[(int)(PL_POS_X + PL_DIR_X * 1)][(int)(PL_POS_Y)] = STEEL_DOOR;
	}

}

static int canMove(int cell)
{
	if (cell == WEAPON_S || cell == WEAPON_C || cell == WEAPON_D || cell == CAN_MOVE || cell == STEEL_DOOR_OPENED)
		return SDL_TRUE;
	return SDL_FALSE;
}

static void moveHero(t_game *pGame, int32_t **pMap, int up)
{
	if (up == SDL_TRUE)
	{
		if (canMove(pMap[(int)(PL_POS_X + PL_DIR_X * 1)][(int)PL_POS_Y]))
			PL_POS_X += PL_DIR_X * 0.25;
		if (canMove(pMap[(int)(PL_POS_X)][(int)(PL_POS_Y + PL_DIR_Y * 1)]))
			PL_POS_Y += PL_DIR_Y * 0.25;
	}
	else
	{
		if (canMove(pMap[(int)(PL_POS_X - PL_DIR_X * 1)][(int)(PL_POS_Y)]))
			PL_POS_X -= PL_DIR_X * 0.25;
		if (canMove(pMap[(int)(PL_POS_X)][(int)(PL_POS_Y - PL_DIR_Y * 1)]))
			PL_POS_Y -= PL_DIR_Y * 0.25;
	}
}

static void rotateCamera(t_game *pGame, const char dir)
{
	double  angle;
	double  tmp;

	angle = 0.07f;
	dir == 'l' ? angle *= -1 : angle;
	tmp = PL_DIR_X;
	PL_DIR_X = PL_DIR_X * cos(-angle) - PL_DIR_Y * sin(-angle);
	PL_DIR_Y = tmp * sin(-angle) + PL_DIR_Y * cos(-angle);
	tmp = pGame->plane.x;
	pGame->plane.x = pGame->plane.x * cos(-angle) - pGame->plane.y * sin(-angle);
	pGame->plane.y = tmp * sin(-angle) + pGame->plane.y * cos(-angle);
}

void    checkEvents(int32_t **pMap, t_game *pGame)
{
	int front;
	int side;

	SDL_Event e;

	front = pMap[(int)(PL_POS_X)][(int)(PL_POS_Y + PL_DIR_Y)];
	side = pMap[(int)(PL_POS_X + PL_DIR_X)][(int)(PL_POS_Y)];
	if (SDL_PollEvent(&e))
	{
		if (e.type == SDL_QUIT || e.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
			exit(EXIT_SUCCESS);

		if (WOLF_KEY == SDL_SCANCODE_1 && e.key.state == SDL_RELEASED)
			pGame->player->weaponType = CHAINSAW;
		if (WOLF_KEY == SDL_SCANCODE_2 && e.key.state == SDL_RELEASED)
			pGame->player->weaponType = SHOTGUN;
		if (WOLF_KEY == SDL_SCANCODE_3 && e.key.state == SDL_RELEASED)
			pGame->player->weaponType = DOUBLEBARREL;
		if ((WOLF_KEY == SDL_SCANCODE_UP || WOLF_KEY == SDL_SCANCODE_W) && e.key.state == SDL_PRESSED)
			moveHero(pGame, pMap, SDL_TRUE);
		else if ((WOLF_KEY == SDL_SCANCODE_DOWN || WOLF_KEY == SDL_SCANCODE_S) && e.key.state == SDL_PRESSED)
			moveHero(pGame, pMap, SDL_FALSE);
		else if (WOLF_KEY == SDL_SCANCODE_LEFT && e.key.state == SDL_PRESSED)
			rotateCamera(pGame, 'l');
		else if (WOLF_KEY == SDL_SCANCODE_RIGHT && e.key.state == SDL_PRESSED)
			rotateCamera(pGame, 'r');
		else if (WOLF_KEY == SDL_SCANCODE_SPACE && e.key.state == SDL_RELEASED)
		{
			if (pGame->player->ammo > 0)
			{
				pGame->player->ammo--;
				pGame->engine->shootAnim = SDL_TRUE;
			}
			else
				pGame->engine->shootAnim = SDL_FALSE;
			checkShotHitBox(pGame, pMap);
		}
		else if (WOLF_KEY == SDL_SCANCODE_E && e.key.state == SDL_RELEASED)
		{
			checkHit(pGame, pMap);
		}
		if (front == WEAPON_C)
		{
			pMap[(int)(PL_POS_X)][(int)(PL_POS_Y + PL_DIR_Y)] = CAN_MOVE;
			pGame->engine->enemies = killSprite(pGame->engine->enemies,
				(t_vec2f){(int)(PL_POS_X), (int)(PL_POS_Y + PL_DIR_Y)});
			pGame->player->weaponType = CHAINSAW;
			pGame->player->score += 100;
		}
		else if (side == WEAPON_C)
		{
			pMap[(int)(PL_POS_X + PL_DIR_X)][(int)(PL_POS_Y)] = CAN_MOVE;
			pGame->engine->enemies = killSprite(pGame->engine->enemies,
				(t_vec2f){(int)(PL_POS_X + PL_DIR_X), (int)(PL_POS_Y)});
			pGame->player->weaponType = CHAINSAW;
			pGame->player->score += 100;
		}
		if (front == WEAPON_S)
		{
			pGame->engine->enemies = killSprite(pGame->engine->enemies,
				(t_vec2f){(int)(PL_POS_X), (int)(PL_POS_Y + PL_DIR_Y)});
			pMap[(int)(PL_POS_X)][(int)(PL_POS_Y + PL_DIR_Y)] = CAN_MOVE;
			pGame->player->weaponType = SHOTGUN;
			pGame->player->score += 100;

		}
		else if (side == WEAPON_S)
		{
			pMap[(int)(PL_POS_X + PL_DIR_X)][(int)(PL_POS_Y)] = CAN_MOVE;
			pGame->engine->enemies = killSprite(pGame->engine->enemies,
				(t_vec2f){(int)(PL_POS_X + PL_DIR_X), (int)(PL_POS_Y)});
			pGame->player->weaponType = SHOTGUN;
			pGame->player->score += 100;
		}


		if (front == WEAPON_D)
		{
			pGame->engine->enemies = killSprite(pGame->engine->enemies,
				(t_vec2f){(int)(PL_POS_X), (int)(PL_POS_Y + PL_DIR_Y)});
			pMap[(int)(PL_POS_X)][(int)(PL_POS_Y + PL_DIR_Y)] = CAN_MOVE;
			pGame->player->weaponType = DOUBLEBARREL;
			pGame->player->score += 100;
		}
		else if (side == WEAPON_D)
		{
			pMap[(int)(PL_POS_X + PL_DIR_X)][(int)(PL_POS_Y)] = CAN_MOVE;
			pGame->engine->enemies = killSprite(pGame->engine->enemies,
				(t_vec2f){(int)(PL_POS_X + PL_DIR_X), (int)(PL_POS_Y)});
			pGame->player->weaponType = DOUBLEBARREL;
			pGame->player->score += 100;
		}

		if (front == ENEMY_HITLER)
		{
//			pGame->player->hp -=20;
		}
		else if (side == ENEMY_HITLER)
		{
//			pGame->player->hp -=20;
		}
		if (pGame->player->hp <= 0)
		{
			fprintf(stderr, "GAME OVER\n");
			exit(1);
		}
//
//		fprintf(stderr, "PosReport: %f:%f (%f:%f) OBJECT %d\n", PL_POS_X, PL_POS_Y, PL_DIR_X, PL_DIR_Y,
//			pMap[(int)PL_POS_X][(int)(PL_POS_Y + PL_DIR_Y * 1)]);
//		fprintf(stderr, "1PosReport: %f:%f (%f:%f) OBJECT %d\n", PL_POS_X, PL_POS_Y, PL_DIR_X, PL_DIR_Y,
//			pMap[(int)(PL_POS_X + PL_DIR_X * 1)][(int)PL_POS_Y]);
	}
}