//
// Created by Oleksii on 06.02.2020.
//

#ifndef WOLF_3D_IRAYCASTER_H
#define WOLF_3D_IRAYCASTER_H


#include <stdbool.h>
#include "../../headers/game.h"
#include "../../headers/defines.h"
#include "g_functions.h"

typedef struct  s_raycast
{
	t_vec2f rayDir;
	t_vec2f sideDist;
	t_vec2f deltaDist;
	t_vec2  map;
	t_vec2  step;

	double  perpWallDist;
	double  cameraX;

	int     side;
	int     lineHeight;
	int     drawStart;
	int     drawEnd;

}               t_raycast;


bool    ddaGetHit(t_game *pGame, t_raycast *pRaycast);
void    getStepDistance(t_game *game, t_raycast *pRaycast);
void    calculateSkyFloor(t_engine *pEngine, t_game *pGame);

int32_t calculateVisionDistance(double distance, int32_t color);

#endif //WOLF_3D_IRAYCASTER_H
