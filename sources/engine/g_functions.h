//
// Created by Oleksii on 06.02.2020.
//

#ifndef WOLF_3D_G_FUNCTIONS_H
# define WOLF_3D_G_FUNCTIONS_H


#include <SDL2_image/SDL_image.h>

#include "../../headers/game.h"
#include "../../headers/defines.h"
#include "IRaycaster.h"
#include "../../userlib/libft/sources/libft.h"

void        clearFrame(uint32_t frame[WIN_HG][WIN_WD]);
void        drawFrame(t_engine *, const uint32_t*);
void        renderWeapon(t_game *pGame);

int         writeTextLine(t_engine*, t_vec2*, const char*, const char*, SDL_Color*);
int         RGBAToInt(SDL_Color c);

SDL_Surface *startShotAnim(t_game *pGame, SDL_Surface **weapon, int delay, uint32_t slides, t_vec2 *pos);
SDL_Color   intToRGBA(uint32_t c);

#endif //WOLF_3D_G_FUNCTIONS_H
