//
// Created by Oleksii on 27.01.2020.
//
#include "sprites.h"

static int loadTexture(const char *pPath, SDL_Surface **pDest)
{
	if (!(*pDest = IMG_Load(pPath)))
	{
		ft_putendl(pPath);
		return 1;
	}

	return 0;
}

static void loadShootGunAnim(t_sprites *pSprites)
{
	unsigned int error;

	error = 0;
	error |= loadTexture( "./sources/assets/anim/shotgun/0.png", &pSprites->anim[0]);
	error |= loadTexture( "./sources/assets/anim/shotgun/1.png", &pSprites->anim[1]);
	error |= loadTexture( "./sources/assets/anim/shotgun/2.png", &pSprites->anim[2]);
	error |= loadTexture( "./sources/assets/anim/shotgun/3.png", &pSprites->anim[3]);
	error |= loadTexture( "./sources/assets/anim/shotgun/4.png", &pSprites->anim[4]);
	error |= loadTexture( "./sources/assets/anim/shotgun/5.png", &pSprites->anim[5]);
	error |= loadTexture( "./sources/assets/anim/shotgun/4.png", &pSprites->anim[6]);
	error |= loadTexture( "./sources/assets/anim/shotgun/3.png", &pSprites->anim[7]);

	error |= loadTexture( "./sources/assets/anim/dBarrel/0.png", &pSprites->sg_anim[0]);
	error |= loadTexture( "./sources/assets/anim/dBarrel/1.png", &pSprites->sg_anim[1]);
	error |= loadTexture( "./sources/assets/anim/dBarrel/2.png", &pSprites->sg_anim[2]);
	error |= loadTexture( "./sources/assets/anim/dBarrel/3.png", &pSprites->sg_anim[3]);
	error |= loadTexture( "./sources/assets/anim/dBarrel/4.png", &pSprites->sg_anim[4]);
	error |= loadTexture( "./sources/assets/anim/dBarrel/5.png", &pSprites->sg_anim[5]);
	error |= loadTexture( "./sources/assets/anim/dBarrel/6.png", &pSprites->sg_anim[6]);
	error |= loadTexture( "./sources/assets/anim/dBarrel/7.png", &pSprites->sg_anim[7]);
	error |= loadTexture( "./sources/assets/anim/dBarrel/8.png", &pSprites->sg_anim[8]);
	error |= loadTexture( "./sources/assets/anim/dBarrel/9.png", &pSprites->sg_anim[9]);
	error |= loadTexture( "./sources/assets/anim/dBarrel/10.png",&pSprites->sg_anim[10]);



	error |= loadTexture( "./sources/assets/anim/chainsaw/0.png", &pSprites->chainSaw[0]);
	error |= loadTexture( "./sources/assets/anim/chainsaw/1.png", &pSprites->chainSaw[1]);
	error |= loadTexture( "./sources/assets/anim/chainsaw/2.png", &pSprites->chainSaw[2]);
	error |= loadTexture( "./sources/assets/anim/chainsaw/3.png", &pSprites->chainSaw[3]);


	if (error)
	{
		ft_putendl("Error: can't load sprites for animation shotgun");
		exit(EXIT_FAILURE);
	}

}

static void loadWalls(t_sprites *pSprites)
{
	unsigned int error;

	error = 0;

	error |= loadTexture( "./sources/assets/bluestone.png", &pSprites->_bwall);

	error |= loadTexture( "./sources/assets/purplestone.png", &pSprites->_gwall); //?

	error |= loadTexture( "./sources/assets/purplestone.png", &pSprites->_pwall);
	error |= loadTexture( "./sources/assets/redbrick.png", &pSprites->_rwall);


	error |= loadTexture( "./sources/assets/purplestone.png", &pSprites->_grass);//?

	error |= loadTexture( "./sources/assets/night.png", &pSprites->_night);


	error |= loadTexture("./sources/assets/walls/1.png", &pSprites->fam[0]);
	error |= loadTexture("./sources/assets/walls/2.png", &pSprites->fam[1]);
	error |= loadTexture("./sources/assets/walls/3.png", &pSprites->fam[2]);
	error |= loadTexture("./sources/assets/walls/4.png", &pSprites->fam[3]);



	if (error)
	{
		ft_putendl("Error: can't load sprites (walls)");
		exit(EXIT_FAILURE);
	}
}
t_sprites   *loadSprites(void)
{
	t_sprites   *sprites;
	uint32_t    error;
	if (!(sprites = (t_sprites *)ft_memalloc(sizeof(t_sprites))))
		exit(EXIT_FAILURE);

	error = 0;

	loadWalls(sprites);
	loadShootGunAnim(sprites);

	error |= loadTexture("./sources/assets/slowpoke.png", &sprites->loadingImage);

	error |= loadTexture("./sources/assets/mm.png", &sprites->_mm);
	error |= loadTexture("./sources/assets/steeldoor.png", &sprites->_door[0]);
	error |= loadTexture("./sources/assets/halfsteeldoor.png", &sprites->_door[1]);
	error |= loadTexture("./sources/assets/elevator.png", &sprites->_elevator);
	error |= loadTexture("./sources/assets/wooden_floor.png", &sprites->_floor[0]);
	error |= loadTexture("./sources/assets/wooden_floor.png", &sprites->_floor[1]);
	error |= loadTexture("./sources/assets/h3.png", &sprites->w_hitler[0]);
	error |= loadTexture("./sources/assets/hitler.png", &sprites->w_hitler[1]);

	error |= loadTexture("./sources/assets/hitler.png", &sprites->_cell[0]);

	error |= loadTexture( "./sources/assets/HUD/hud.png", &sprites->hud);
	error |= loadTexture( "./sources/assets/HUD/hp_bar.png", &sprites->hp[0]);
	error |= loadTexture( "./sources/assets/HUD/hp_bar1.png",&sprites->hp[1]);
	error |= loadTexture( "./sources/assets/HUD/hp_bar2.png",&sprites->hp[2]);

	error |= loadTexture( "./sources/assets/sprites/chainsaw.png",&sprites->weapon[0]);
	error |= loadTexture( "./sources/assets/sprites/dBarrel.png", &sprites->weapon[1]);
	error |= loadTexture( "./sources/assets/sprites/shotgun.png", &sprites->weapon[2]);

	error |= loadTexture( "./sources/assets/sprites/hitler/0.png", &sprites->e_hitler[0]);
	error |= loadTexture( "./sources/assets/sprites/hitler/1.png", &sprites->e_hitler[1]);
	error |= loadTexture( "./sources/assets/sprites/hitler/2.png", &sprites->e_hitler[2]);
	error |= loadTexture( "./sources/assets/sprites/hitler/3.png", &sprites->e_hitler[3]);
	error |= loadTexture( "./sources/assets/sprites/hitler/4.png", &sprites->e_hitler[4]);
	error |= loadTexture( "./sources/assets/sprites/hitler/5.png", &sprites->e_hitler[5]);
	error |= loadTexture( "./sources/assets/sprites/hitler/6.png", &sprites->e_hitler[6]);
	error |= loadTexture( "./sources/assets/sprites/hitler/5.png", &sprites->e_hitler[7]);
	error |= loadTexture( "./sources/assets/sprites/hitler/6.png", &sprites->e_hitler[8]);



	error |= loadTexture( "./sources/assets/sprites/demon/0.png", &sprites->e_demon[0]);
	error |= loadTexture( "./sources/assets/sprites/demon/1.png", &sprites->e_demon[1]);
	error |= loadTexture( "./sources/assets/sprites/demon/2.png", &sprites->e_demon[2]);
	error |= loadTexture( "./sources/assets/sprites/demon/3.png", &sprites->e_demon[3]);
	error |= loadTexture( "./sources/assets/sprites/demon/4.png", &sprites->e_demon[4]);
	error |= loadTexture( "./sources/assets/sprites/demon/5.png", &sprites->e_demon[5]);
	error |= loadTexture( "./sources/assets/sprites/demon/6.png", &sprites->e_demon[6]);
	error |= loadTexture( "./sources/assets/sprites/demon/7.png", &sprites->e_demon[7]);

	error |= loadTexture( "./sources/assets/sprites/demon/d0.png", &sprites->e_demon_d[0]);
	error |= loadTexture( "./sources/assets/sprites/demon/d1.png", &sprites->e_demon_d[1]);
	error |= loadTexture( "./sources/assets/sprites/demon/d2.png", &sprites->e_demon_d[2]);
	error |= loadTexture( "./sources/assets/sprites/demon/d3.png", &sprites->e_demon_d[3]);
	error |= loadTexture( "./sources/assets/sprites/demon/d4.png", &sprites->e_demon_d[4]);
	error |= loadTexture( "./sources/assets/sprites/demon/d5.png", &sprites->e_demon_d[5]);
	error |= loadTexture( "./sources/assets/sprites/demon/d6.png", &sprites->e_demon_d[6]);
	error |= loadTexture( "./sources/assets/sprites/demon/d7.png", &sprites->e_demon_d[7]);


	error |= loadTexture( "./sources/assets/sprites/hitler/d0.png", &sprites->e_hitler_d[0]);
	error |= loadTexture( "./sources/assets/sprites/hitler/d1.png", &sprites->e_hitler_d[1]);
	error |= loadTexture( "./sources/assets/sprites/hitler/d2.png", &sprites->e_hitler_d[2]);



	sprites->_cell[0]       = IMG_Load("./sources/assets/pillar.png");

	if (error)
	{
		fprintf(stderr, "IMG_Load: %s\n", IMG_GetError());
		exit(EXIT_FAILURE);
	}
	return sprites;
}


void processSpriteList(t_list *sprites)
{
	t_list *it;
	int type;

	it = sprites;
	while (it)
	{
		type = DR_SPRITE->type;
		if (type == ENEMY_HITLER)
		{
			DR_SPRITE->health = 100;
			DR_SPRITE->isDead = SDL_FALSE;
			DR_SPRITE->texture = ((t_sprites *)DR_SPRITE->pSpr)->e_hitler;
			DR_SPRITE->spriteSize = DR_SPRITE->texture[0]->w;
			DR_SPRITE->animLength = 9;
			DR_SPRITE->vDiv = 1;
			DR_SPRITE->uDiv = 1;
		}
		else if (type == ENEMY_DEAMON)
		{
			DR_SPRITE->health = 200;
			DR_SPRITE->isDead = SDL_FALSE;
			DR_SPRITE->texture = ((t_sprites *)DR_SPRITE->pSpr)->e_demon;
			DR_SPRITE->spriteSize = DR_SPRITE->texture[0]->w;
			DR_SPRITE->animLength = 8;
			DR_SPRITE->vDiv = 1;
			DR_SPRITE->uDiv = 1;
		}
		else if (type == WEAPON_C)
		{
			DR_SPRITE->health = 0;
			DR_SPRITE->isDead = SDL_TRUE;
			DR_SPRITE->texture = &((t_sprites *)DR_SPRITE->pSpr)->weapon[0];
			DR_SPRITE->spriteSize = DR_SPRITE->texture[0]->w;
			DR_SPRITE->animLength = 1;
			DR_SPRITE->vDiv = 2;
			DR_SPRITE->uDiv = 2;
		}
		else if (type == WEAPON_D)
		{
			DR_SPRITE->health = 0;
			DR_SPRITE->isDead = SDL_TRUE;
			DR_SPRITE->texture = &((t_sprites *)DR_SPRITE->pSpr)->weapon[1];
			DR_SPRITE->spriteSize = DR_SPRITE->texture[0]->w;
			DR_SPRITE->animLength = 1;
			DR_SPRITE->vDiv = 2;
			DR_SPRITE->uDiv = 2;
		}
		else if (type == WEAPON_S)
		{
			DR_SPRITE->health = 0;
			DR_SPRITE->isDead = SDL_TRUE;
			DR_SPRITE->texture = &((t_sprites *)DR_SPRITE->pSpr)->weapon[2];
			DR_SPRITE->spriteSize = DR_SPRITE->texture[0]->w;
			DR_SPRITE->animLength = 1;
			DR_SPRITE->vDiv = 2;
			DR_SPRITE->uDiv = 2;
		}
		it = it->next;
	}
}

t_list  *initSprites(int32_t **map, t_sprites *resources)
{
	t_list      *sprites;
	t_sprite    sprite;
	int         x;
	int         y;

	x = -1;
	sprites = NULL;
	while (++x < MAX_MAP_SIZE)
	{
		y = -1;
		while (++y < MAX_MAP_SIZE)
		{
			if (map[x][y] == ENEMY_HITLER || map[x][y] == WEAPON_C || map[x][y] == WEAPON_S
			    || map[x][y] == WEAPON_D || map[x][y] == ENEMY_DEAMON)
			{
				sprite.pos = (t_vec2f){x, y};
				sprite.pSpr = resources;
				sprite.type = map[x][y];
				if (!sprites)
					sprites = ft_lstnew(&sprite, sizeof(t_sprite));
				else
					ft_lstadd(&sprites, ft_lstnew(&sprite, sizeof(t_sprite)));
			}
		}
	}
	processSpriteList(sprites);
	return sprites;
}

