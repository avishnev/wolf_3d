//
// Created by Oleksii on 06.02.2020.
//

#include "g_functions.h"

void        drawFrame(t_engine *pEngine, const uint32_t *pFrameBuffer)
{
	uint32_t    *bufp;
	int         y;
	int         x;

	bufp = (uint32_t *)pEngine->surface->pixels;
	y = -1;
	while (++y < WIN_HG)
	{
		x = -1;
		while (++x < WIN_WD)
		{
			*bufp = pFrameBuffer[y * WIN_WD + x];
			bufp++;
		}
		bufp += pEngine->surface->pitch / 4;
		bufp -= WIN_WD;
	}
}

void        clearFrame(uint32_t frame[WIN_HG][WIN_WD])
{
	int x;
	int y;

	y = -1;
	while (++y < WIN_HG)
	{
		x = -1;
		while (++x < WIN_WD)
			frame[y][x] = 0x0;
	}
}


static bool ticksReached(uint32_t pTime)
{
	static uint32_t lastTime;
	static uint32_t currentTime;

	currentTime = SDL_GetTicks();
	if (currentTime > lastTime + pTime)
	{
		lastTime = currentTime;
		return SDL_TRUE;
	}
	return SDL_FALSE;
}

SDL_Surface *startShotAnim(t_game *pGame, SDL_Surface **weapon, int delay, uint32_t slides, t_vec2 *pos)
{
	SDL_Surface     *ret;
	static uint32_t frame;

	ret = NULL;
	if (pGame->engine->shootAnim)
	{
		ret = weapon[frame];
		if (ticksReached(delay))
			frame++;
	}
	else
		ret = weapon[0];
	*pos = (t_vec2){(HALF_WD) - weapon[0]->w * 0.5, WIN_HG - weapon[0]->h};
	if (frame == slides)
	{
		pGame->engine->shootAnim = SDL_FALSE;
		frame = 0;
	}
	return ret;
}

int         writeTextLine(t_engine *engine, t_vec2 *p, const char *line, const char *font, SDL_Color *color)
{
	SDL_Surface *tmp;

	if (!engine || !(engine->font = TTF_OpenFont(font, color->a)))
	{
		ft_putstr(IMG_GetError());
		return 0;
	}
	color->a = 0xff;
	tmp = TTF_RenderText_Solid(engine->font, line, *color);
	SDL_BlitSurface(tmp, NULL, engine->surface, &(SDL_Rect){p->x, p->y, tmp->w , tmp->h});
	SDL_FreeSurface(tmp);
	TTF_CloseFont(engine->font);

	return 1;
}


int         RGBAToInt(SDL_Color c)
{
	return ((c.r & 0xff) << 24) | ((c.g & 0xff) << 16) | ((c.b & 0xff) << 8) | (c.a & 0xff);
}

SDL_Color   intToRGBA(uint32_t c)
{
	return (SDL_Color){(c >> 24) & 0xff,(c >> 16) & 0xff, (c >> 8) & 0xff, c & 0xff};
}


void renderWeapon(t_game *pGame)
{
	SDL_Surface *wpSprite;
	t_vec2      pos;

	if (pGame->player->weaponType == DOUBLEBARREL)
		wpSprite = startShotAnim(pGame, pGame->engine->res->sg_anim, 120, 10, &pos);
	else if (pGame->player->weaponType == SHOTGUN)
		wpSprite = startShotAnim(pGame, pGame->engine->res->anim, 120, 7, &pos);
	else
		wpSprite = startShotAnim(pGame, pGame->engine->res->chainSaw, 120, 3, &pos);
	SDL_BlitSurface(wpSprite, NULL, pGame->engine->surface, &(SDL_Rect) {pos.x, pos.y, wpSprite->w, wpSprite->h});
}

