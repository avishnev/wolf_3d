//
// Created by Oleksii on 13.04.2020.
//
#include "utils.h"
#include "../../headers/defines.h"

t_list  *killSprite(t_list *it, t_vec2f pos)
{
	t_list *tmpN;

	if (!it)
		return NULL;
	if (DR_SPRITE->pos.x == pos.x && DR_SPRITE->pos.y == pos.y)
	{
		if (DR_SPRITE->type == ENEMY_HITLER || DR_SPRITE->type == ENEMY_DEAMON)
		{
			DR_SPRITE->health <= 0 ? DR_SPRITE->health -= 20 : 0;
			if (!DR_SPRITE->health)
				DR_SPRITE->isDead = SDL_TRUE;
		}
		else if (DR_SPRITE->type == WEAPON_C || DR_SPRITE->type == WEAPON_D || DR_SPRITE->type == WEAPON_S)
		{
			tmpN = it->next;
			WOLF_FREE(it);
			return tmpN;
		}
	}
	it->next = killSprite(it->next, pos);
	return it;
}

static t_list  *SortedMerge(t_list *a, t_list *b)
{
	t_list  *result;

	if (!a)
		return (b);
	else if (!b)
		return (a);

	if  (((t_sprite *)a->content)->dist >= ((t_sprite *)b->content)->dist)
	{
		result = a;
		result->next = SortedMerge(a->next, b);
	}
	else
	{
		result = b;
		result->next = SortedMerge(a, b->next);
	}
	return (result);
}

static void     FrontBackSplit(t_list *source, t_list **frontRef, t_list **backRef)
{
	t_list  *fast;
	t_list  *slow;

	slow = source;
	fast = source->next;
	while (fast)
	{
		fast = fast->next;
		if (fast)
		{
			slow = slow->next;
			fast = fast->next;
		}
	}
	*frontRef = source;
	*backRef = slow->next;
	slow->next = NULL;
}

void    calculateDistance(t_list **enemies, t_vec2f pos)
{
	t_list *it;

	it = *enemies;
	while (it)
	{
		((t_sprite *)it->content)->dist = ((pos.x - ((t_sprite *)it->content)->pos.x) *
		(pos.x - ((t_sprite *)it->content)->pos.x) + (pos.y - ((t_sprite *)it->content)->pos.y) *
		(pos.y - ((t_sprite *)it->content)->pos.y));
		it = it->next;
	}
}

void    MergeSort(t_list **headRef)
{
	t_list  *head;
	t_list  *a;
	t_list  *b;

	head = *headRef;
	if ((!head) || (!head->next)) {
		return;
	}
	FrontBackSplit(head, &a, &b);
	MergeSort(&a);
	MergeSort(&b);
	*headRef = SortedMerge(a, b);
}
