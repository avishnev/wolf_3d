//
// Created by Oleksii on 13.04.2020.
//

#ifndef WOLF_3D_UTILS_H
#define WOLF_3D_UTILS_H

#include "../../userlib/libft/sources/libft.h"
#include "../../headers/data_types.h"

t_list  *killSprite(t_list *it, t_vec2f pos);
void    MergeSort(t_list **);
void    calculateDistance(t_list **, t_vec2f);

#endif //WOLF_3D_UTILS_H
