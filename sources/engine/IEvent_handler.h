//
// Created by Oleksii on 21.02.2020.
//

#ifndef WOLF_3D_IEVENT_HANDLER_H
#define WOLF_3D_IEVENT_HANDLER_H


#include "SDL2/SDL.h"
#include "../../headers/game.h"

void    checkEvents(int32_t **, t_game*);

#endif //WOLF_3D_IEVENT_HANDLER_H
