//
// Created by Oleksii on 27.01.2020.
//

#ifndef WOLF_3D_SPRITES_H
#define WOLF_3D_SPRITES_H

#include <SDL2_image/SDL_image.h>
#include "../../userlib/libft/sources/libft.h"
#include "../../headers/defines.h"
#include "../../headers/data_types.h"

typedef struct  s_sprites
{
	SDL_Surface *loadingImage;
	SDL_Surface *hud;
	SDL_Surface *_bwall;
	SDL_Surface *_rwall;
	SDL_Surface *_pwall;
	SDL_Surface *_gwall;
	SDL_Surface *fam[4];
	SDL_Surface *_mm;

	SDL_Surface *_elevator;
	SDL_Surface *_door[3];
	SDL_Surface *w_hitler[2];
	SDL_Surface *_grass;
	SDL_Surface *_night;
	SDL_Surface *_floor[6];
	SDL_Surface *_cell[6];

	SDL_Surface *e_hitler[9];
	SDL_Surface *e_hitler_d[3];
	SDL_Surface *e_demon[8];
	SDL_Surface *e_demon_d[8];

	SDL_Surface *anim[9];
	SDL_Surface *chainSaw[4];
	SDL_Surface *sg_anim[11];
	SDL_Surface *hp[3];

	SDL_Surface *weapon[3];
}               t_sprites;


t_sprites   *loadSprites(void);
t_list      *initSprites(int32_t **, t_sprites *);

#endif //WOLF_3D_SPRITES_H
