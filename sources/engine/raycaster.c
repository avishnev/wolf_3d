//
// Created by Oleksii on 06.02.2020.
//
#include "IRaycaster.h"

static int isDrawable(int m)
{
	if (!m || m == WEAPON_D || m == WEAPON_S || m == WEAPON_C || m == ENEMY_HITLER || m == ENEMY_DEAMON )
		return 0;
	return 1;

}
bool ddaGetHit(t_game *pGame, t_raycast *pRaycast)
{
	if (pRaycast->sideDist.x < pRaycast->sideDist.y)
	{
		pRaycast->sideDist.x += pRaycast->deltaDist.x;
		pRaycast->map.x += pRaycast->step.x;
		pRaycast->side = 0;
	}
	else
	{
		pRaycast->sideDist.y += pRaycast->deltaDist.y;
		pRaycast->map.y += pRaycast->step.y;
		pRaycast->side = 1;
	}

	return (isDrawable(pGame->engine->mapType[pRaycast->map.x][pRaycast->map.y]));
}

void getStepDistance(t_game *pGame, t_raycast *pRaycast)
{
	if (pRaycast->rayDir.x < 0)
	{
		pRaycast->step.x = -1;
		pRaycast->sideDist.x = (PL_POS_X - pRaycast->map.x) * pRaycast->deltaDist.x;
	}
	else
	{
		pRaycast->step.x = 1;
		pRaycast->sideDist.x = ((float)pRaycast->map.x + 1.0f - PL_POS_X) * pRaycast->deltaDist.x;
	}
	if (pRaycast->rayDir.y < 0)
	{
		pRaycast->step.y = -1;
		pRaycast->sideDist.y = (PL_POS_Y - pRaycast->map.y) * pRaycast->deltaDist.y;
	}
	else
	{
		pRaycast->step.y = 1;
		pRaycast->sideDist.y = ((float)pRaycast->map.y + 1.0f - PL_POS_Y) * pRaycast->deltaDist.y;
	}
}


int32_t calculateVisionDistance(double distance, int32_t color)
{
	SDL_Color r;
	uint8_t shift;

	if (distance < 4.8f)
		return color;
	shift = 1;
	if (distance > 5.0f)
		shift++;
	if (distance > 5.2f)
		shift++;
	if (distance > 5.4f)
		shift++;
	if (distance > 5.6f)
		shift++;

	r = intToRGBA(color);
	return RGBAToInt((SDL_Color){r.r >> shift, r.g >> shift, r.b >> shift, r.a >> shift});
}


void    calculateSkyFloor(t_engine *pEngine, t_game *pGame) //TODO: SKYBOX
{
	int y;
	const float halfScreen = WIN_HG >> 1;

	uint32_t  *pText[2];
	t_vec2f rayDir;
	t_vec2f rayDir1;
	t_vec2f floorCord;
	t_vec2f floorStep;

	pText[0] = pGame->engine->res->_floor[1]->pixels;
	pText[1] = pGame->engine->res->_floor[0]->pixels;
	y = halfScreen - 1;

	while (++y < WIN_HG)
	{
		rayDir  = (t_vec2f){PL_DIR_X - pGame->plane.x, PL_DIR_Y - pGame->plane.y};
		rayDir1 = (t_vec2f){PL_DIR_X + pGame->plane.x, PL_DIR_Y + pGame->plane.y};

		// Horizontal distance from the camera to the floor for the current row.
		// 0.5 is the z position exactly in the middle between floor and ceiling.
		float rowDistance = halfScreen /  (y - halfScreen);

		// calculate the real world step vector we have to add for each x (parallel to camera plane)
		// adding step by step avoids multiplications with a weight in the inner loop
		floorStep  = (t_vec2f) {rowDistance * (rayDir1.x - rayDir.x) / WIN_WD,
		                        rowDistance * (rayDir1.y - rayDir.y) / WIN_WD};

		// real world coordinates of the leftmost column. This will be updated as we step to the right.

		floorCord = (t_vec2f){PL_POS_X + rowDistance * rayDir.x, PL_POS_Y + rowDistance * rayDir.y};
		int x = -1;

		while (++x < WIN_WD)
		{
			t_vec2 cell = (t_vec2){(int)(floorCord.x), (int)(floorCord.y)};
			// the cell coord is simply got from the integer parts of floorX and floorY
#ifdef NTEXTURERESOLUTION
#undef NTEXTURERESOLUTION
#define NTEXTURERESOLUTION 64
#endif
			t_vec2 tc = (t_vec2){(int)(NTEXTURERESOLUTION * (floorCord.x - cell.x)) & (NTEXTURERESOLUTION - 1),
			                     (int)(NTEXTURERESOLUTION * (floorCord.y - cell.y)) & (NTEXTURERESOLUTION - 1)};

			// get the texture coordinate from the fractional part

			floorCord.x += floorStep.x;
			floorCord.y += floorStep.y;

			// choose texture and draw the pixel
			uint32_t color;
			if (rowDistance < OBSERVED_DIST)
			{
				// floor
				color = pText[0][NTEXTURERESOLUTION * tc.y | tc.x];
				color = calculateVisionDistance(rowDistance, color);
				pEngine->frameBuffer[y][x] = color;

				color = pText[1][NTEXTURERESOLUTION * tc.y | tc.x];
				color = calculateVisionDistance(rowDistance, color);
				pEngine->frameBuffer[WIN_HG - y - 1][x] = color;
			}
			else
			{
				pEngine->frameBuffer[WIN_HG - y - 1][x] = 0;
				pEngine->frameBuffer[y][x] = 0;
			}
		}
	}

}
