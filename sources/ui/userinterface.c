//
// Created by Oleksii on 17.01.2020.
//
#include "IUserinterface.h"
#include "../engine/g_functions.h"

# define RED_CLR   &(SDL_Color) {205, 50 ,50, 30}
# define GREEN_CLR &(SDL_Color) {50, 255 ,50, 30}


static void renderHelmet(t_engine *engine)
{
	SDL_BlitSurface(engine->res->hud, NULL, engine->surface, &(SDL_Rect){0,0, WIN_WD, WIN_HG});
}


static void initValues(char *ui[2], t_player *pUi)
{
	ui[0] = ft_itoa(pUi->ammo);
	ui[1] = ft_itoa(pUi->score);
}
void        drawUI(t_engine *engine, t_player *pUi)
{
	char *ui[2];
	const char *font = "sources/fonts/BoldItalic.ttf";

	initValues(ui, pUi);
	renderHelmet(engine);
	if (pUi->hp >= 100)
		SDL_BlitSurface(engine->res->hp[0], NULL, engine->surface, &(SDL_Rect)
		{WIN_WD * 0.14, WIN_HG - 90, 0, 0});
	else if (pUi->hp < 100 && pUi->hp >= 50)
		SDL_BlitSurface(engine->res->hp[1], NULL, engine->surface, &(SDL_Rect)
		{WIN_WD * 0.14, WIN_HG - 90, 0, 0});
	else if (pUi->hp < 50 && pUi->hp >= 25)
		SDL_BlitSurface(engine->res->hp[2], NULL, engine->surface, &(SDL_Rect)
		{WIN_WD * 0.14, WIN_HG - 90, 0, 0});
	writeTextLine(engine, &(t_vec2){WIN_WD * 0.71, WIN_HG - 70}, "AMMO  ", font, RED_CLR);
	writeTextLine(engine, &(t_vec2){WIN_WD * 0.71, WIN_HG - 95}, "SCORE ", font, RED_CLR);
	writeTextLine(engine, &(t_vec2){WIN_WD * 0.80, WIN_HG - 70}, ui[0], font, GREEN_CLR);
	writeTextLine(engine, &(t_vec2){WIN_WD * 0.80, WIN_HG - 95}, ui[1], font, GREEN_CLR);
	free(ui[0]);
	free(ui[1]);
}
