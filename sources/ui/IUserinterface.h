//
// Created by Oleksii on 17.01.2020.
//

#ifndef WOLF_3D_USERINTERFACE_H
# define WOLF_3D_USERINTERFACE_H

# include <SDL_surface.h>
# include "../../headers/defines.h"
# include "../../headers/game.h"


void drawUI(t_engine *, t_player *);


#endif //WOLF_3D_USERINTERFACE_H
