// Created by ovishnevskiy on 11/12/19.
//
#include <SDL2/SDL_audio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#include <math.h>

#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <SDL2_ttf/SDL_ttf.h>
#include <SDL2_mixer/SDL_mixer.h>


#include "../userlib/libft/sources/libft.h"
#include "../headers/defines.h"

#include "../headers/game.h"
#include "ui/IUserinterface.h"
#include "parser/IParser.h"

#include "engine/sprites.h"
#include "engine/IRaycaster.h"
#include "engine/g_functions.h"

#include "engine/IEvent_handler.h"
#include "engine/utils.h"

uint32_t    *getCombineTextureIndex(t_sprites *pSprites, int side, t_vec2f dir, int type)
{
	(void)type;

	if (!side && dir.x > 0)
		return pSprites->_rwall->pixels;
	else if (!side && dir.x < 0)
		return pSprites->_gwall->pixels;
	else if (side && dir.y > 0)
		return pSprites->_bwall->pixels;
	else
		return pSprites->w_hitler[0]->pixels;
}

uint32_t *getTextureIndex(int32_t **pMap, t_vec2 *pPos, t_sprites *pSprites, t_raycast *pR)
{
	if (!pMap || !pPos)
		exit(EXIT_FAILURE);
	if (pMap[pPos->x][pPos->y] == BRICK_WALL_RED)
		return pSprites->_rwall->pixels;
	if (pMap[pPos->x][pPos->y] == GRAY_STONE)
		return pSprites->_gwall->pixels;
	if (pMap[pPos->x][pPos->y] == BLUE_WALL)
		return pSprites->_bwall->pixels;
	if (pMap[pPos->x][pPos->y] == ELEVATOR)
		return pSprites->_elevator->pixels;
	if (pMap[pPos->x][pPos->y] == GRASS_DAY)
		return pSprites->_grass->pixels;
	if (pMap[pPos->x][pPos->y] == NIGHT_SHIT)
		return pSprites->_night->pixels;
	if (pMap[pPos->x][pPos->y] == STEEL_DOOR)
		return pSprites->_door[0]->pixels;
	if (pMap[pPos->x][pPos->y] == STEEL_DOOR_OPENED)
		return pSprites->_door[1]->pixels;
	if (pMap[pPos->x][pPos->y] == HITLER)
		return pSprites->w_hitler[0]->pixels;
	if (pMap[pPos->x][pPos->y] == PENIS_HITLER)
		return pSprites->w_hitler[1]->pixels;
	if (pMap[pPos->x][pPos->y] == COMBINE_TEXTURE)
		return getCombineTextureIndex(pSprites, pR->side, pR->rayDir, COMBINE_TEXTURE);
	if (pMap[pPos->x][pPos->y] == WALL_FAM)
		return pSprites->fam[TICK % 4]->pixels;
	return 0;
}


double ZBuffer[WIN_WD];

void showFps(t_engine *pEngine)
{
	static unsigned	int	fps;
	static unsigned	int	time_current;
	static unsigned	int	time_past;
	static unsigned	int	ticks;
	static char			*fps_str;

	time_current = time(NULL);
	if (time_current - time_past && (time_past = time_current))
	{
		fps = 1000 / (SDL_GetTicks() - ticks);
		ft_memdel((void**)&fps_str);
		fps_str = ft_itoa((int)fps);
	}
	ticks = SDL_GetTicks();
	if (fps >= 50)
	{
		if (!writeTextLine(pEngine, &(t_vec2){pEngine->surface->w - 50, 10}, fps_str,
		"/System/Library/Fonts/Supplemental/Apple Chancery.ttf", &(SDL_Color){GREEN_C, 20})) //TODO: chnage to local font
			exit(EXIT_FAILURE);
	}
	else
	{
		if (!writeTextLine(pEngine, &(t_vec2){pEngine->surface->w - 50, 10}, fps_str,
		"/System/Library/Fonts/Supplemental/Apple Chancery.ttf", &(SDL_Color){RED_C, 20})) //TODO: chnage to local font
			exit(EXIT_FAILURE);
	}
}

t_vec2f getPlayerPosition(int32_t **map)
{
	int y;
	int x;

	y = 0;
	while (y < MAX_MAP_SIZE)
	{
		x = 0;
		while (x < MAX_MAP_SIZE)
		{
			if (map[y][x] == PLAYER)
			{
				map[y][x] = CAN_MOVE;
				return (t_vec2f) {((double)y + 0.25f), ((double)x + 0.25f)};
			}
			x++;
		}
		y++;
	}
	ft_putstr("Error: can`t find started player position\n");
	exit(EXIT_FAILURE);
}

void loadingShow(t_engine *engine)
{
	if (engine->loaded == TRUE)
		return;
	if (!writeTextLine(engine, &(t_vec2){engine->surface->w / 5 - 100, engine->surface->h - 150},
       "Guy`s i`ve done WOLF :D", "/System/Library/Fonts/Supplemental/Chalkduster.ttf", &(SDL_Color) { //TODO: chnage to local font
		RED_C, 50}))
		exit(EXIT_FAILURE);
	SDL_BlitSurface(engine->res->loadingImage, NULL, engine->surface, &(SDL_Rect){
			engine->surface->w / 4, engine->surface->h / 4,0, 0});
	SDL_UpdateWindowSurface(engine->window);
	SDL_FreeSurface(engine->res->loadingImage);
	SDL_Delay(2000);
	SDL_FillRect(engine->surface, NULL, 0x0);
	SDL_UpdateWindowSurface(engine->window);

}

void runMainMenu(t_engine *engine)
{
	SDL_Event e;

	SDL_BlitSurface(engine->res->_mm, NULL, engine->surface,  &(SDL_Rect){0, 30,100, 100});
	SDL_UpdateWindowSurface(engine->window);
	while (SDL_WaitEvent(&e))
	{
		if (e.type == SDL_QUIT || e.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
			exit(EXIT_SUCCESS);
		if (e.key.keysym.scancode == SDL_SCANCODE_X)
			break;
	}
	engine->loaded = TRUE;
	SDL_FreeSurface(engine->res->_mm);
	SDL_FillRect(engine->surface, NULL, 0x0);
	SDL_UpdateWindowSurface(engine->window);
}

void renderWeapon(t_game *pGame);

void    renderAll(t_engine *pEngine, t_game *pGame)
{
	if (!pEngine->loaded)
	{
		loadingShow(pEngine);
		runMainMenu(pEngine);
	}
	showFps(pEngine);
	SDL_UpdateWindowSurface(pEngine->window);
	drawFrame(pEngine, *pEngine->frameBuffer);
	drawUI(pEngine, pGame->player);
	renderWeapon(pGame);
	clearFrame(pEngine->frameBuffer);
}

void calculateSprites(t_game *pGame);

void calculateWalls(t_raycast *rc, t_game *pGame)
{
	uint32_t *ret;
	int x;

	x = -1;
	while (++x < WIN_WD)
	{
		rc->cameraX = 2 * x / (double)WIN_WD - 1;
		rc->perpWallDist = 0;

		rc->rayDir = (t_vec2f){PL_DIR_X + pGame->plane.x * rc->cameraX,PL_DIR_Y + pGame->plane.y * rc->cameraX};

		rc->map = (t_vec2){ (int)PL_POS_X, (int)PL_POS_Y};

		rc->sideDist = (t_vec2f){0, 0};
		rc->deltaDist  = (t_vec2f){fabs(1 / rc->rayDir.x),fabs(1 / rc->rayDir.y)};
		rc->side = 0;

		getStepDistance(pGame, rc);

		while (!ddaGetHit(pGame, rc));

		if (!rc->side)
			rc->perpWallDist = (rc->map.x - PL_POS_X + (double)(1 - rc->step.x) / 2) / rc->rayDir.x;
		else
			rc->perpWallDist = (rc->map.y - PL_POS_Y + (double)(1 - rc->step.y) / 2) / rc->rayDir.y;
		ZBuffer[x] = rc->perpWallDist;
		if (rc->perpWallDist > OBSERVED_DIST)
			continue;
		rc->lineHeight = (int)(WIN_HG / rc->perpWallDist);

		rc->drawStart = (-rc->lineHeight) / 2 + (HALF_HG);
		if (rc->drawStart < 0)
			rc->drawStart = 0;
		rc->drawEnd = rc->lineHeight / 2 + (HALF_HG);
		if (rc->drawEnd >= WIN_HG)
			rc->drawEnd = WIN_HG - 1;

		double wallX;
		if (rc->side == 0)
			wallX = PL_POS_Y + rc->perpWallDist * rc->rayDir.y;
		else
			wallX = PL_POS_X + rc->perpWallDist * rc->rayDir.x;

		wallX -= floor((wallX));

		int texX = (int)(wallX * (double)(NTEXTURERESOLUTION));
		if (rc->side == 0 && rc->rayDir.x > 0)
			texX = NTEXTURERESOLUTION - texX - 1;
		if (rc->side == 1 && rc->rayDir.y < 0)
			texX = NTEXTURERESOLUTION - texX - 1;

		double step = 1.0 * NTEXTURERESOLUTION / rc->lineHeight;
		double texPos = (rc->drawStart - (HALF_HG) + rc->lineHeight / 2.0) * step;

		for (int y = rc->drawStart; y < rc->drawEnd; y++)
		{
			uint32_t texY = (uint32_t)texPos & (uint32_t)(NTEXTURERESOLUTION - 1);
			texPos += step;

			if (rc->perpWallDist < OBSERVED_DIST)
			{
				uint32_t color = 0;
				ret = getTextureIndex(pGame->engine->mapType, &rc->map, pGame->engine->res, rc);
				if (!ret)
					break;
				color = calculateVisionDistance(rc->perpWallDist, ret[NTEXTURERESOLUTION * texY + texX]);
				FRAME_B[y][x] = color;
			}
			else
				FRAME_B[y][x] = 0;
		}
	}
}

int runGame(t_game *pGame)
{
	bool        endgame;

	endgame = false;

	while (!endgame)
	{
		calculateSkyFloor(pGame->engine, pGame);
		calculateWalls(pGame->engine->rc, pGame);
		calculateSprites(pGame);
		checkEvents(pGame->engine->mapType, pGame);
		renderAll(pGame->engine, pGame);
	}
	return FALSE;
}

typedef struct  s_spritedata
{
	t_vec2f sprite;
	t_vec2f transform;
	t_vec2f spriteScreen;
	t_vec2f drawStart;
	t_vec2f drawEnd;
	int spriteHeight;
	int spriteWidth;
}               t_spritedata;

#define SPRITESIZE 130

void calculateSprites(t_game *pGame)
{
	t_list *it;

	calculateDistance(&pGame->engine->enemies, pGame->player->position);
	MergeSort(&pGame->engine->enemies);
	it = pGame->engine->enemies;
	while (it)
	{
		//translate sprite position to relative to camera
		double spriteX = DR_SPRITE->pos.x - PL_POS_X;
		double spriteY = DR_SPRITE->pos.y - PL_POS_Y;

		double invDet = 1.0 / (pGame->plane.x * PL_DIR_Y - PL_DIR_X * pGame->plane.y); //required for correct matrix multiplication

		double transformX = invDet * (PL_DIR_Y * spriteX - PL_DIR_X * spriteY);
		double transformY = invDet * (-pGame->plane.y * spriteX + pGame->plane.x * spriteY); //this is actually the depth inside the screen, that what Z is in 3D

		int spriteScreenX = (int)((HALF_HG) * (1 + transformX / transformY));

		int vMoveScreen = (int)(DR_SPRITE->spriteSize/ transformY);

		int spriteHeight = abs((int)(WIN_HG / (transformY))) / (DR_SPRITE->vDiv);

		int drawStartY = -spriteHeight / 2 + (HALF_HG) + vMoveScreen;
		if (drawStartY < 0)
			drawStartY = 0;
		int drawEndY = spriteHeight / 2 + (HALF_HG) + vMoveScreen;
		if (drawEndY >= WIN_WD)
			drawEndY = WIN_WD - 1;

		int spriteWidth = abs( (int)(WIN_WD / (transformY))) / DR_SPRITE->uDiv;

		int drawStartX = -spriteWidth / 2 + spriteScreenX;
		if (drawStartX < 0)
			drawStartX = 0;

		int drawEndX = spriteWidth / 2 + spriteScreenX;

		if (drawEndX >= WIN_WD)
			drawEndX = WIN_WD - 1;

		for (int stripe = drawStartX; stripe < drawEndX; stripe++)
		{
			int spSize = DR_SPRITE->spriteSize;
			int texX = (int)(256 * (stripe - (-spriteWidth / 2 + spriteScreenX)) * spSize / spriteWidth) / 256;
			if (transformY > 0 && stripe > 0 && stripe < WIN_WD && transformY < ZBuffer[stripe])
			{
				for (int y = drawStartY; y < drawEndY; y++)
				{
					int d = (y - vMoveScreen) * 256 - WIN_HG * 128 + spriteHeight * 128;
					int texY = ((d * spSize) / spriteHeight) / 256;
					Uint32 color = ((uint32_t *)(DR_SPRITE->texture[TICK % DR_SPRITE->animLength]->pixels))[spSize * texY + texX];
					if ((color & 0xFFFFFFFA) != 0)
						FRAME_B[y][stripe] = calculateVisionDistance(DR_SPRITE->dist / 5, color);
				}
			}
		}
		it = it->next;
	}
}


int initGameData(t_game *pGame, t_sprites *resources)
{
	pGame->player->position = getPlayerPosition(pGame->engine->mapType);
	if (pGame->player->position.x < 1.0 || pGame->player->position.y < 1.0)
	{
		ft_putendl("Error: wrong player position");
		return 0;
	}
	pGame->player->weaponType = SHOTGUN;
	pGame->player->hp = 100;
	pGame->player->direction = (t_vec2f){1.f,0.f };
	pGame->plane = (t_vec2f){0.f,-0.60};
	pGame->engine->rc->map = (t_vec2){PL_POS_X, PL_POS_Y};
	pGame->player->ammo = 100;
	pGame->player->score = 0;
	pGame->engine->enemies = initSprites(pGame->engine->mapType, resources);
	return 1;
}

int initGraphicLibrary(t_game *pGame)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0 || TTF_Init() < 0 || (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) < 0))
		exit(EXIT_FAILURE);

	pGame->engine->window = SDL_CreateWindow("WOLF_3D", SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED, WIN_WD, WIN_HG, SDL_WINDOW_ALLOW_HIGHDPI);
	pGame->engine->surface = SDL_GetWindowSurface(pGame->engine->window);

	pGame->engine->res = loadSprites();
	pGame->engine->loaded = FALSE;
	return (1);
}

int allocator(t_game *src, int fd)
{
	src->player = (t_player *)ft_memalloc(sizeof(t_player));
	src->engine = (t_engine *)ft_memalloc(sizeof(t_engine));

	src->engine->mapType = createMapType(fd);
	src->engine->rc = (t_raycast *)ft_memalloc(sizeof(t_raycast));

	if (!src->player || !src->engine)
		return 0;
	return 1;
}

int cleanupAll(t_game *pGame)
{
	if (pGame)
	{
		if (pGame->engine)
			WOLF_FREE(pGame->engine->rc);
		WOLF_FREE(pGame->player);
		freeMapType(pGame->engine->mapType);
	}
	return (EXIT_SUCCESS);
}

int main(int ac, const char **av)
{
	t_game  game;

	if (!allocator(&game, open_file(checkInput(ac, av))))
		exit(EXIT_FAILURE);
	initGraphicLibrary(&game);
	if (!initGameData(&game, game.engine->res))
		exit(EXIT_FAILURE);

	runGame(&game);

	SDL_DestroyWindow(game.engine->window);
	SDL_Quit();
	IMG_Quit();
	TTF_Quit();
	return (cleanupAll(&game));

}
