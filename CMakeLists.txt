cmake_minimum_required(VERSION 3.5)
project(wolf_3d)

set(CMAKE_C_FLAGS ${CMAKE_C_FLAGS} "-Wall -Wextra -g -O0")

set(PROJECT_SRC
        sources/main.c
        sources/main.c
        sources/parser/parser.c
        headers/defines.h
        sources/parser/IParser.h
        headers/gameengine.h
        headers/game.h
        sources/ui/userinterface.c
        sources/ui/IUserinterface.h
        sources/engine/sprites.c
        sources/engine/sprites.h
        sources/engine/raycaster.c
        sources/engine/IRaycaster.h
        sources/engine/graphic_functions.c
        sources/engine/g_functions.h
        sources/engine/event_handler.c
        sources/engine/utils.c)
#SDL
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")

find_package(SDL2 REQUIRED)

include_directories(${SDL2_INCLUDE_DIRS} ${SDL2IMAGE_INCLUDE_DIRS})
if (NOT SDL2_FOUND)
    message(WARNING "SDL2 not found!")
endif (NOT SDL2_FOUND)

add_executable(${PROJECT_NAME} ${PROJECT_SRC})

if (NOT EXISTS "${CMAKE_SOURCE_DIR}/libft.a")
    message("COMPILE LIBS")

    add_custom_command(OUTPUT wolf_3d
            COMMAND make
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/userlib/libft/sources
            )
endif()
set(SDL_CUSTOM_FRAMEWORKS ${SDL_CUSTOM_FRAMEWORKS} /Library/Frameworks/SDL2_mixer.framework
        /Library/Frameworks/SDL2_image.framework
        /Library/Frameworks/SDL2_ttf.framework
        )

target_link_libraries(${PROJECT_NAME}  ${SDL2_LIBRARIES} ${SDL_CUSTOM_FRAMEWORKS} ${CMAKE_SOURCE_DIR}/userlib/libft.a)
